from fastapi import FastAPI, Body, Path, Query, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.databasse import Session, engine, Base
from middlewares.error_handler import ErrorHandler
from models.computadoras import Computadora as ComputadoraModel

app = FastAPI()
app.title = "-- COMPUTADORAS --"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
Base.metadata.create_all(bind=engine)

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>-- Computadoras --</h1>')

class Computadora(BaseModel):
    id: Optional[int]
    marca: str = Field(default="Marca Computadora", min_length=2, max_length=15)
    modelo: str = Field(default="Modelo Computadora", min_length=2, max_length=15)
    color: str = Field(default="Color Computadora", min_length=2, max_length=15)
    ram: int = Field(default=4)  # Tipo de dato corregido a entero
    almacenamiento: str = Field(default="Almacenamiento Computadora", min_length=2, max_length=15)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "marca": "Mi Computadora",
                "modelo": "Modelo de la Computadora",
                "color": "Color de la Computadora",
                "ram": 16,  # Tipo de dato corregido a entero
                "almacenamiento": "512GB SSD"
            }
        }

@app.get('/computadoras', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def get_computadoras() -> List[Computadora]:
    db = Session()
    result = db.query(ComputadoraModel).all()
    return result

@app.get('/computadoras/{id}', tags=['computadoras'], response_model=Computadora)
def get_computadora_by_id(id: int) -> Computadora:
    db = Session()
    computadora = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not computadora:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    return computadora

@app.get('/computadoras/marcas/{marca}', tags=['computadoras'], response_model=List[Computadora])
def get_computadoras_by_marca(marca: str = Path(..., min_length=5, max_length=15)):
    db = Session()
    data = db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
    return data

@app.post('/computadoras', tags=['computadoras'], status_code=201)
def create_computadora(computadora: Computadora):
    db = Session()
    new_computadora = ComputadoraModel(**computadora.dict())
    db.add(new_computadora)
    db.commit()
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la computadora"})

@app.delete('/computadoras/{id}', tags=['computadoras'], status_code=200)
def delete_computadora(id: int):
    db = Session()
    data = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not data:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    db.delete(data)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})

@app.put('/computadoras/{id}', tags=['computadoras'], status_code=200)
def update_computadora(id: int, computadora: Computadora):
    db = Session()
    result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not result:
        db.close()
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})    
    result.marca = computadora.marca
    result.modelo = computadora.modelo
    result.color = computadora.color
    result.ram = computadora.ram
    result.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha actualizado la computadora"})
